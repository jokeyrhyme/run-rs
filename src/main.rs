#![deny(clippy::all)]

mod bidirectional_cycle;

use iced::widget::text_input::{focus, move_cursor_to_end, Id};
use iced::widget::{container, text_input};
use iced::window::{close, request_user_attention, UserAttention};
use iced::{executor, keyboard, subscription, Color, Event, Length, Subscription};
use iced::{Application, Command, Element, Settings, Theme};

use bidirectional_cycle::BidirectionalCycle;

// TODO: extract out TextInputWithAutocomplete widget

fn main() -> iced::Result {
    Run::run(Settings {
        id: Some(String::from("me.jokeyrhy.run")),
        default_font: Some(monospace()),
        window: iced::window::Settings {
            decorations: false,
            position: iced::window::Position::Centered,
            resizable: false,
            size: (320, 32),
            transparent: true,
            ..Default::default()
        },
        ..Default::default()
    })
}

#[derive(Clone, Debug)]
enum Message {
    Event(Event),
    TextInputChanged(String),
    TextInputSubmitted,
}

#[derive(Debug)]
enum Mode {
    Completed(BidirectionalCycle<String>),
    UserInput,
}

struct Run {
    input_id: Id,
    mode: Mode,
    value: String,
}

impl Application for Run {
    type Executor = executor::Default;
    type Flags = ();
    type Message = Message;
    type Theme = Theme;

    fn new(_flags: ()) -> (Run, Command<Self::Message>) {
        // TODO: focus app window (in a wayland-friendly way)
        let r = Run::default();
        let id = r.input_id.clone();
        (
            r,
            Command::batch([
                focus(id),
                request_user_attention(Some(UserAttention::Informational)),
            ]),
        )
    }

    fn subscription(&self) -> Subscription<Self::Message> {
        subscription::events().map(Message::Event)
    }

    fn theme(&self) -> Theme {
        Theme::custom(iced::theme::Palette {
            // https://rosepinetheme.com/palette/ingredients
            background: Color::from_rgb8(25, 23, 36), // base
            text: Color::from_rgb8(224, 222, 244),    // text
            primary: Color::from_rgb8(235, 188, 186), // rose
            success: Color::from_rgb8(49, 116, 143),  // pine
            danger: Color::from_rgb8(235, 111, 146),  // love
        })
    }

    fn title(&self) -> String {
        String::from("run")
    }

    fn update(&mut self, msg: Self::Message) -> Command<Self::Message> {
        match msg {
            // TODO: implement readline-style keyboard shortcuts e.g. Ctrl-U
            Message::Event(Event::Keyboard(keyboard::Event::KeyPressed {
                key_code: keyboard::KeyCode::Tab,
                modifiers: keyboard::Modifiers::SHIFT,
            })) => {
                match &mut self.mode {
                    Mode::Completed(bc) => {
                        if let Some(suggestion) = bc.next_back() {
                            self.value = suggestion;
                        }
                    }
                    Mode::UserInput => { /* noop, wrong mode, do nothing */ }
                }
                move_cursor_to_end(self.input_id.clone())
            }
            Message::Event(Event::Keyboard(keyboard::Event::KeyPressed {
                key_code: keyboard::KeyCode::Tab,
                ..
            })) => {
                match &mut self.mode {
                    Mode::Completed(bc) => {
                        if let Some(suggestion) = bc.next() {
                            self.value = suggestion;
                        }
                    }
                    Mode::UserInput => {
                        let mut suggestions = BidirectionalCycle::new(complete(&self.value));
                        if let Some(s) = suggestions.next() {
                            self.value = s;
                            self.mode = Mode::Completed(suggestions);
                        }
                    }
                }
                move_cursor_to_end(self.input_id.clone())
            }
            Message::Event(Event::Keyboard(
                keyboard::Event::KeyPressed {
                    key_code: keyboard::KeyCode::Escape,
                    ..
                }
                | keyboard::Event::KeyReleased {
                    key_code: keyboard::KeyCode::Escape,
                    ..
                },
            )) => close(),
            // | Message::Event(Event::Window(iced::window::Event::Unfocused))
            Message::TextInputSubmitted => {
                if let Err(e) = execute(&self.value) {
                    eprintln!("{:?}", e);
                }
                close()
            }
            Message::TextInputChanged(s) => {
                self.mode = Mode::UserInput;
                self.value = s;
                Command::none()
            }
            _ => Command::none(),
        }
    }

    fn view(&self) -> Element<Self::Message> {
        // TODO: remove border around the text input
        let input = text_input("command line input", &self.value)
            .id(self.input_id.clone())
            .on_input(Message::TextInputChanged)
            .on_submit(Message::TextInputSubmitted)
            .padding(8)
            .size(24);

        // TODO: remove whitespace around the text input
        container(input)
            .center_x()
            .center_y()
            .height(Length::Fill)
            .width(Length::Fill)
            .into()
    }
}

impl Default for Run {
    fn default() -> Self {
        Self {
            input_id: Id::unique(),
            mode: Mode::UserInput,
            value: String::default(),
        }
    }
}

fn complete(input: &str) -> Vec<String> {
    let re = if let Ok(re) = regex::Regex::new(input) {
        re
    } else {
        return vec![];
    };
    if input.chars().any(char::is_whitespace) {
        // don't bother trying to match a command once arguments are in the mix
        return vec![];
    }
    match which::which_re(re) {
        Ok(matches) => matches
            .filter_map(|p| p.file_name().and_then(|o| o.to_str()).map(String::from))
            .collect(),
        _ => vec![],
    }
}

fn execute(input: &str) -> Result<(), std::io::Error> {
    if input.trim().is_empty() {
        return Ok(());
    }
    let parts: Vec<&str> = input.split_ascii_whitespace().collect();
    let (exe, args): (&str, &[&str]) = match &parts[0..] {
        [exe, args @ ..] if !args.is_empty() => (exe, args),
        [exe] => (exe, &[]),
        _ => {
            return Ok(());
        }
    };

    std::process::Command::new(exe)
        .args(args)
        .spawn()
        .map(|_| ())
}

fn monospace() -> &'static [u8] {
    let font_source = iced_graphics::font::Source::new();
    let font = font_source
        .load(&[
            iced_graphics::font::Family::Title(String::from("Iosevka")),
            iced_graphics::font::Family::Monospace,
        ])
        .expect("unable to load fonts");
    let font = Box::new(font);
    font.leak()
}
