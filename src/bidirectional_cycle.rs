/// endless like [`Cycle`](std::iter::Cycle) but backwards and forwards,
/// backwards and forwards like [`DoubleEndedIterator`](std::iter::DoubleEndedIterator) but endless
#[derive(Debug)]
pub(crate) struct BidirectionalCycle<T> {
    /// None: we have not started iterating yet
    cursor: Option<usize>,
    items: Vec<T>,
}

impl<T> BidirectionalCycle<T>
where
    T: Clone,
{
    pub(crate) fn new(items: Vec<T>) -> Self {
        Self {
            cursor: None,
            /// important to take ownership of `items` as we do not want anything else to modify it
            items,
        }
    }

    pub(crate) fn next_back(&mut self) -> Option<T> {
        if self.items.is_empty() {
            return None;
        }
        match self.cursor {
            Some(c) => {
                if c == 0 {
                    self.cursor = Some(self.items.len() - 1);
                } else {
                    self.cursor = Some(c.wrapping_sub(1));
                }
            }
            None => {
                self.cursor = Some(0);
            }
        }
        // unwrap: above ensures that it's always `Some`
        Some(self.items[self.cursor.unwrap()].clone())
    }
}

impl<T> Iterator for BidirectionalCycle<T>
where
    T: Clone,
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.items.is_empty() {
            return None;
        }
        match self.cursor {
            Some(c) => {
                if c >= (self.items.len() - 1) {
                    self.cursor = Some(0);
                } else {
                    self.cursor = Some(c.wrapping_add(1));
                }
            }
            None => {
                self.cursor = Some(0);
            }
        }
        // unwrap: above ensures that it's always `Some`
        Some(self.items[self.cursor.unwrap()].clone())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty_items() {
        let input: Vec<u32> = vec![];
        let mut iter = BidirectionalCycle::new(input);

        assert_eq!(iter.next(), None);
        assert_eq!(iter.next_back(), None);
    }

    #[test]
    fn next_wraps_to_start() {
        let input: Vec<&str> = vec!["one", "two", "three"];
        let mut iter = BidirectionalCycle::new(input);

        assert_eq!(iter.next(), Some("one"));
        assert_eq!(iter.next(), Some("two"));
        assert_eq!(iter.next(), Some("three"));
        assert_eq!(iter.next(), Some("one"));
        assert_eq!(iter.next(), Some("two"));
    }

    #[test]
    fn next_back_wraps_to_end() {
        let input: Vec<&str> = vec!["one", "two", "three"];
        let mut iter = BidirectionalCycle::new(input);

        assert_eq!(iter.next_back(), Some("one"));
        assert_eq!(iter.next_back(), Some("three"));
        assert_eq!(iter.next_back(), Some("two"));
        assert_eq!(iter.next_back(), Some("one"));
    }

    #[test]
    fn mix_of_next_and_next_back() {
        let input: Vec<&str> = vec!["one", "two", "three"];
        let mut iter = BidirectionalCycle::new(input);

        assert_eq!(iter.next(), Some("one"));
        assert_eq!(iter.next_back(), Some("three"));
        assert_eq!(iter.next(), Some("one"));
        assert_eq!(iter.next(), Some("two"));
        assert_eq!(iter.next_back(), Some("one"));
        assert_eq!(iter.next_back(), Some("three"));
        assert_eq!(iter.next_back(), Some("two"));
        assert_eq!(iter.next(), Some("three"));
    }
}
