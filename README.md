# run-rs [![Status](https://img.shields.io/badge/status-actively--developed-brightgreen)](https://gitlab.com/jokeyrhyme/run-rs) [![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/jokeyrhyme/run-rs?branch=main)](https://gitlab.com/jokeyrhyme/run-rs/-/pipelines?ref=main)

command runner prompt for Linux desktops

## what?

- this project is mostly a toy for me to learn desktop GUI development with [iced](https://iced.rs/)

- when run, it displays a small window asking for you to type in a command

- when you hit Enter/Return, it tries to run whatever you typed in, that's it!

## see also

- https://github.com/Cloudef/bemenu

- https://tools.suckless.org/dmenu/

- https://hg.sr.ht/~scoopta/wofi
